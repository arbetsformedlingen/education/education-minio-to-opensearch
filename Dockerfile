FROM docker.io/library/python:3.9.20-slim-bookworm

RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install -y git build-essential && \
    apt-get clean -y

RUN apt-get -y update && apt-get -y install jq && apt-get clean
COPY . /opensearchimporter/
WORKDIR /opensearchimporter
RUN pip install -r requirements.txt

CMD [ "python3", "/opensearchimporter/docker_starter.py" ]
