# Education Minio to opensearch

Stores file data into opensearch as documents in an index.

## Podman

Podman (and Docker) are great for running stuff independently of your local system.
The commands in this read me can changed to Docker by switching `podman` for `docker`.

### Build

To run the tool with Podman, first you need to build the image.

```shell
podman build -t educationminiotoopensearch:latest .
```

### Run

Make sure to update the `docker-local.env`` file with credentials for the S3 storage. If your OpenSearch instance has authentication, update that as well.

```shell
podman run -rm --env-file=docker-local.env --network=host educationminiotoopensearch:latest
```

#### Networking and localhost in containers

Running the container with `--network=host` will result in `127.0.0.1` and `localhost` resolving to the host machine instead of the container.

If it's not possible to use `--network=host` use the address `host.docker.internal` which will resolve to the container host.

### Debug Podman

To start the image with a shell you can run the following command:

```shell
podman run -it --rm educationminiotoopensearch:latest /bin/bash
```
