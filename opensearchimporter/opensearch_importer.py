import logging

from education_opensearch.opensearch_store import OpensearchStore

from opensearchimporter import settings
from opensearchimporter.minio_downloader import MinioDownloader

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)


def opensearch_import():
    educations_batch_size = int(settings.EDUCATIONS_BATCH_SIZE)
    log.info(f'Starting to import educations from Minio to Opensearch with batch size: {educations_batch_size}')
    minio_downloader = MinioDownloader()

    best_match_file = minio_downloader.find_best_match_file()

    opensearch_store = OpensearchStore()
    index_name = opensearch_store.start_new_save(settings.ES_EDUCATIONS_ALIAS,
                                                 mappings=settings.MINIO_TO_OPENSEARCH_MAPPINGS)

    education_counter = 0
    educations_batch = []
    for education in minio_downloader.download_educations(best_match_file):
        education_counter += 1
        educations_batch.append(education)

        if len(educations_batch) == educations_batch_size:
            import_batch(educations_batch, opensearch_store)
            educations_batch = []
    # Import remaining educations
    if len(educations_batch) > 0:
        import_batch(educations_batch, opensearch_store)

    opensearch_store.create_or_update_alias_for_index(index_name, settings.ES_EDUCATIONS_ALIAS)
    log.info(f'Stored {education_counter} educations in Opensearch.')


def import_batch(educations_batch, opensearch_store):
    try:
        opensearch_store.save_items_in_repository(educations_batch)
    except Exception as e:
        log.error(f"Error when saving to opensearch {e}")
